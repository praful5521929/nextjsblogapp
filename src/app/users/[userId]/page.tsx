import React, { Suspense } from 'react'
import getUser from '../../../../lib/getUsers'
import getUserPosts from '../../../../lib/getUserPosts'
import UserPost from './components/UserPost'
import getAllUsers from '../../../../lib/getAllUsers'

import { notFound } from 'next/navigation'

type Params = {
    params:{
        userId:string
    }
}

export default async function UserPage({params}:any) {
    const {userId}=params
    
    const userData:Promise<any>=getUser(userId)
    const userPostsData:Promise<any>=getUserPosts(userId)

    // const [user,userPosts]=await Promise.all([userData,userPostsData])

    const user=await userData

    if(!user?.name) return notFound()

    // console.log(user);
    
  return (
    // <div>page</div>
    <>
        <h2>{user.name}</h2>
        <br />      
        <Suspense fallback={<h2>Loading ... </h2>}>
            <UserPost promise={userPostsData}/>
        </Suspense>
    </>
  )
}
export async function generateStaticParams() {
    const userData = getAllUsers()
    const users=await userData
   
    return users.map((post:any) => ({
      userId: post.id.toString(),
    }))
  }