import Image from 'next/image'
import Link from 'next/link'
import { Router } from 'next/router';

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
      <h1>Home Page</h1>
      <Link href={'/about'}>go to aboutus page</Link>
      <Link href={'/users'} >go to users page</Link>
    </main>
  )
}
export const metadata = {
  title: "Home",
  description: "This is Home page.",
  keywords: ["next.js", "react", "javascript"],
};
