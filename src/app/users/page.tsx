import React from "react";
import type { Metadata } from "next";
import getAllUsers from "../../../lib/getAllUsers";
import Link from "next/link";

export default async function UsersPage() {
  const userData = getAllUsers();
  const users = await userData;
  
  const content = (
    <section>
      <h2>
        <Link href={"/"}>Back to home</Link>
      </h2>
      <br />
      {users?.map((user: any) => {
        return (
          <>
            <p key={user?.id}>
                <Link href={`/users/${user.id}`}>
                    {user?.name}
                </Link>
            </p>
            <br />
          </>
        );
      })}
    </section>
  );
  return content;
}
export const metadata: Metadata = {
  title: "User",
  description: "This is user page.",
  keywords: ["next.js", "react", "javascript"],
};
