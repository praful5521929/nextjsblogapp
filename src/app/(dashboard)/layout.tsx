import Footer from "../components/Footer/page";
import Header from "../components/Header/page";

export default function DashboadLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="flex flex-col min-h-screen">
      <Header />
      <div className="bg-gray-900 w-full grow">
        {children}
      </div>
      <Footer />
    </div>
  );
}
