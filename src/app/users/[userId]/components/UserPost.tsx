type Props = {
    promise:Promise<any[]>
}

export default async function UserPost({promise}:Props) {
    const posts= await promise
    
    const content = posts.map((post:any,index:number)=>{
        return(
            <article key={post?.id}> 
                <h2>{post?.title}</h2>
                <p>{post?.body}</p>
                <hr />
            </article>
        )
    })
    // const content=<div></div>
  return content
}
