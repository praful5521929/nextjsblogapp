import React from 'react'

export default async function getUserPosts(userId:string) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts?userId=${userId}`,{cache:'no-store'});
    if (!response.ok) {
      return undefined
    }
    return response.json();
}
